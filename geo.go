package main

import (
	"flag"
	"github.com/ip2location/ip2location-go"
	"github.com/olivere/elastic/v7"
)

var (
	ip2geo *ip2location.DB
)

func init() {
	flag.Parse()
	var err error
	ip2geo, err = ip2location.OpenDB(*geoPath)
	if err != nil {
		panic(err)
	}
}

func IPGeo(ip string) (*Geo, error) {
	all, err := ip2geo.Get_all(ip)
	if err != nil {
		return nil, err
	}
	return &Geo{
		Country: all.Country_short,
		Region:  all.Region,
		City:    all.City,
		Geo: elastic.GeoPoint{
			Lat: float64(all.Latitude),
			Lon: float64(all.Longitude),
		},
	}, nil
}
