package main

import (
	"github.com/olivere/elastic/v7"
	"time"
)

type Geo struct {
	Country string           `json:"country"`
	Region  string           `json:"region"`
	City    string           `json:"city"`
	Geo     elastic.GeoPoint `json:"geo"`
}

type Asn struct {
	From int
	To   int
	Cidr string
	Asn  string
	As   string
}

type Tcppc struct {
	Timestamp time.Time `json:"timestamp"`
	Flow      struct {
		Proto string `json:"proto"`
		Src   string `json:"src"`
		Sport int    `json:"sport"`
		Dst   string `json:"dst"`
		Dport int    `json:"dport"`
	} `json:"flow"`
	Payloads []struct {
		Index     int       `json:"index"`
		Timestamp time.Time `json:"timestamp"`
		Data      string    `json:"data"`
	} `json:"payloads"`
	Attacker struct {
		Country string           `json:"country"`
		Region  string           `json:"region"`
		City    string           `json:"city"`
		Geo     elastic.GeoPoint `json:"geo"`
		Rdns    string           `json:"rdns"`
		Asn     string           `json:"asn"`
		As      string           `json:"as"`
	} `json:"attacker"`
}
