package main

import (
	"encoding/binary"
	"encoding/csv"
	"flag"
	"net"
	"os"
	"strconv"
)

var (
	ip2asn      []Asn
	ip2asnCount int
)

func init() {
	flag.Parse()
	file, err := os.OpenFile(*asnPath, os.O_RDONLY, 0600)
	if err != nil {
		panic(err)
	}
	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		panic(err)
	}
	for _, line := range records {
		fromStr := line[0]
		toStr := line[1]
		from, _ := strconv.Atoi(fromStr)
		to, _ := strconv.Atoi(toStr)
		asn := Asn{
			From: from,
			To:   to,
			Cidr: line[2],
			Asn:  line[3],
			As:   line[4],
		}
		ip2asn = append(ip2asn, asn)
	}
	ip2asnCount = len(ip2asn)
}

func IPAsn(ip string) (string, string) {
	ipInt := int(ip2int(net.ParseIP(ip)))
	pointer := ip2asnCount / 2
	max := ip2asnCount
	min := 0
	for {
		t := ip2asn[pointer]
		if ipInt >= t.From {
			if ipInt <= t.To {
				return t.Asn, t.As
			}
			min = pointer
			pointer = (min + max) / 2
		} else {
			max = pointer
			pointer = (min + max) / 2
		}
	}
}

func ip2int(ip net.IP) uint32 {
	if len(ip) == 16 {
		return binary.BigEndian.Uint32(ip[12:16])
	}
	return binary.BigEndian.Uint32(ip)
}
