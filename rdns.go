package main

import "net"

func IPRDns(ip string) (string, error) {
	addr, err := net.LookupAddr(ip)
	if err != nil {
		return "", err
	}
	return addr[0], nil
}
