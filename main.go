package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/olivere/elastic/v7"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"time"
)

const mapping = `
{
    "mappings": {
        "properties": {
            "attacker.geo": {
                "type": "geo_point"
            },
            "attacker.rdns": {
                "type": "text",
                "analyzer": "pattern"
            }
        }
    }
}`

var (
	wg   sync.WaitGroup
	pool []*Tcppc
	lock sync.Mutex
)

func main() {
	ctx := context.Background()
	now := time.Now()

	indice := fmt.Sprintf("honeypot-%d%.2d", now.Year(), now.Month())
	es, err := elastic.NewClient(elastic.SetURL(*esUrl))
	if err != nil {
		panic(err)
	}
	exists, err := es.IndexExists(indice).Do(ctx)
	if err != nil {
		panic(err)
	}
	if !exists {
		_, err := es.CreateIndex(indice).BodyString(mapping).Do(ctx)
		if err != nil {
			panic(err)
		}
	}

	file, err := ioutil.ReadFile(*logPath)
	if err != nil {
		panic(err)
	}
	err = truncate(*logPath)
	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(file), "\n") {
		if line == "" {
			continue
		}
		var tcppc Tcppc
		err := json.Unmarshal([]byte(line), &tcppc)
		if err != nil {
			fmt.Printf("json.Unmarshal - %s - %v \n", line, err)
		}
		wg.Add(1)
		go run(&tcppc)
	}
	wg.Wait()

	bulk := es.Bulk()
	for _, each := range pool {
		bulk.Add(elastic.NewBulkIndexRequest().Index(indice).Doc(each))
	}
	_, err = bulk.Do(ctx)
	if err != nil {
		panic(err)
	}
}

func truncate(filename string) error {
	f, err := os.OpenFile(filename, os.O_TRUNC, 0644)
	if err != nil {
		return fmt.Errorf("could not open file %q for truncation: %v", filename, err)
	}
	if err = f.Close(); err != nil {
		return fmt.Errorf("could not close file handler for %q after truncation: %v", filename, err)
	}
	return nil
}

func run(tcppc *Tcppc) {
	defer wg.Done()
	ip := tcppc.Flow.Src
	geo, _ := IPGeo(ip)
	rdns, _ := IPRDns(ip)
	asn, as := IPAsn(ip)
	tcppc.Attacker.Country = geo.Country
	tcppc.Attacker.Region = geo.Region
	tcppc.Attacker.City = geo.City
	tcppc.Attacker.Geo = geo.Geo
	tcppc.Attacker.Rdns = rdns
	tcppc.Attacker.Asn = asn
	tcppc.Attacker.As = as
	lock.Lock()
	pool = append(pool, tcppc)
	lock.Unlock()
}
