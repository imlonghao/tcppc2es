package main

import (
	"flag"
)

var (
	geoPath = flag.String("g", "./IP2LOCATION-LITE-DB5.BIN", "path to the ip2location geo db.")
	asnPath = flag.String("a", "./IP2LOCATION-LITE-ASN.CSV", "path to the ip2location asn db.")
	logPath = flag.String("l", "/var/log/tcppc/tcppc.jsonl", "path to the tcppc log.")
	esUrl   = flag.String("e", "http://127.0.0.1:9200", "elasticsearch url.")
)

func init() {
	flag.Parse()
}
