module gitlab.com/imlonghao/tcppc2es

go 1.15

require (
	github.com/ip2location/ip2location-go v8.3.0+incompatible
	github.com/olivere/elastic/v7 v7.0.19
)
