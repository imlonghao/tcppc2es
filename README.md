# tcppc2es

Parse and send tcppc honeypot log to elasticsearch

## How it work?

1. Read all the tcppc honeypot log to memory
2. Truncate the honeypot log
3. Parse every line of the log, add geo / asn / rdns information of the attacker
4. Bulk insert indexes to Elasticsearch

## Requirements

- IP2Location LITE IP Address Geolocation Database DB5
- IP2Location LITE IP-ASN Database
- Elasticsearch 7.x
- tcppc-go

## Usage

```
Usage of ./tcppc2es:
  -a string
        path to the ip2location asn db. (default "./IP2LOCATION-LITE-ASN.CSV")
  -e string
        elasticsearch url. (default "http://127.0.0.1:9200")
  -g string
        path to the ip2location geo db. (default "./IP2LOCATION-LITE-DB5.BIN")
  -l string
        path to the tcppc log. (default "/var/log/tcppc/tcppc.jsonl")
```

## License

MIT